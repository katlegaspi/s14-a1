console.log("Hello World");

// variables
let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"]
let workAddress = {
	city: "Lincoln",
	houseNumber: "32",
	state: "Nebraska",
	street: "Washington"
}

// person
function person(firstName, lastName, age, hobbies, workAddress){
	console.log(firstName + " " + lastName + " is " + age + " years of age.");
	console.log("This was printed inside of the function");
	console.log(hobbies);
	console.log("This was printed inside of the function");
	console.log(workAddress);
}

// return isMarried: true
function returnMarried(){
	return true;
}

let isMarried = returnMarried();

// calls
console.log("First Name: " + firstName);
console.log("Last Name: " + firstName);
console.log("Age: " + age);

console.log("Hobbies: ");
console.log(hobbies);

console.log("Work Address:");
console.log(workAddress);

person(firstName, lastName, age, hobbies, workAddress);

console.log("The value of isMarried is: " + isMarried);